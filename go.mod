module gitlab.com/1f320/pronouns

go 1.18

require (
	emperror.dev/errors v0.8.1
	github.com/Masterminds/squirrel v1.5.2
	github.com/bwmarrin/discordgo v0.25.0
	github.com/georgysavva/scany v0.3.0
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/render v1.0.1
	github.com/golang-jwt/jwt/v4 v4.4.1
	github.com/jackc/pgconn v1.12.0
	github.com/jackc/pgx/v4 v4.16.0
	github.com/joho/godotenv v1.4.0
	github.com/mediocregopher/radix/v4 v4.1.0
	github.com/rs/xid v1.2.1
	github.com/rubenv/sql-migrate v1.1.1
	go.uber.org/zap v1.21.0
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602
)

require (
	github.com/go-gorp/gorp/v3 v3.0.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.11.0 // indirect
	github.com/jackc/puddle v1.2.1 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/tilinna/clock v1.0.2 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
